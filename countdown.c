///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   @todo
//
// @author Vinton Sistoza <sistozav@wiliki>
// @date   2/4/20
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

struct tm{
   int yrs,days,hrs,mins,secs;
};

int main(int argc, char* argv[])
{
   int flag,y,d,h,m,s;
   printf("Type '1' to countdown, '2' to countup\n");
   scanf("%d",&flag);
   printf("Reference Time: Tue Jan 21 04:26:07 PM HST 2014\n");

   struct tm time;
   time.yrs = 7; 
   time.days = 12; 
   time.hrs = 6;
   time.mins = 20;
   time.secs = 55;

   y = time.yrs;
   d = time.days;
   h = time.hrs;
   m = time.mins;
   s = time.secs;
   
   //Countdown or Countup
   if(flag==1)
   {
      while(1)
      {
         printf("Years: %d Days: %d Hours: %d Minutes: %d Seconds: %d\n",y,d,h,m,s);
         sleep(1);
         s++;
         if(s>59){
            s=0;
            m++;
         }
         else if(m>59){
            m=0;
            h++;
         }
         else if(h>23){
            h=0;
            d++;
         }
         else if(d>364){
            d=0;
            y++;
         }
      }
   }
   else
   {
      while(1)
      {
         printf("Years: %d Days: %d Hours: %d Minutes: %d Seconds: %d\n",y,d,h,m,s);
         sleep(1);
         s--;
         if(s<0){
            s=60;
            m--;
         }
         else if(m<0){
            m=59;
            h--;
         }
         else if(h<0){
            h=23;
            d--;
            }
         else if(d<0){
            d=364;
            y--;
         }

      }

   }

   return 0;
}
